# go-optional

Yet another nullable/optional solution for Go primitives.

Each Go primitives gets a wrapper structure that saves the underlying value and a flag whether it is set.

```go
// Int wraps type int as an optional.
type Int struct {
	v  int
	ok bool
}
```

The wrappers expose a set of functions each.

- `NewType(v)` creates a wrapper initialized to some value.
- `Type{}` creates an empty wrapper (Go construct, not a function).
- `Type.Set(v)` sets the wrapper to some value.
- `Type.Get()` returns the content and flag of the wrapper.
- `Type.Clear()` empties the wrapper.
- `MarshalJSON()/UnmarshalJSON()` for compatiblity with `encoding/json`.
- `String()` for pretty-printing.
